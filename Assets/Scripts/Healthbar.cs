using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healthbar : MonoBehaviour
{
  public GameObject healthBar;
  private float health;
  private float full = 1.0f;
  private float empty = 0.0f;

  public float damage(float amount)
  {
    Debug.Log("Damage");
    Debug.Log(amount);
    if (health - amount < empty) health = empty;
    else health -= amount;
    return health;
  }

  void Start()
  {
    Vector2 originalScale = new Vector2(full, healthBar.transform.localScale.y);
    healthBar.transform.localScale = originalScale;

    health = full;
  }

  void Update()
  {
    if (health <= empty) Debug.Log("DEAD :(");
    if (health >= empty) {
      Vector2 newScale = new Vector2(health, healthBar.transform.localScale.y);
      healthBar.transform.localScale = newScale;
    }
  }
}
