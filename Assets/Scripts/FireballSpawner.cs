using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class FireballSpawner : MonoBehaviour
{
  public GameObject dragonHead;
  public GameObject fireballSource;
  public GameObject fireball;
  public float timeBetweenShots = 1;
  private float originalTimeBetweenShots;
  /* private Animator dragonHeadAnimation; */

  void Start()
  {
    /* dragonHeadAnimation = dragonHead.GetComponent<Animator>(); */
    originalTimeBetweenShots = timeBetweenShots;
  }

  void Update()
  {
    if (timeBetweenShots > 0) timeBetweenShots -= Time.deltaTime;
    else if (Input.GetMouseButtonDown(0)) ShootFireball();
  }

  void ShootFireball()
  {
    Vector3 mouseClickFromCamera = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    mouseClickFromCamera.z = 0;
    Vector3 angleToClick = dragonHead.transform.position - mouseClickFromCamera;

    GameObject spawnedFireball = Instantiate(fireball, fireballSource.transform.position, Quaternion.identity);
    spawnedFireball.transform.rotation = Quaternion.LookRotation(Vector3.forward, angleToClick);

    timeBetweenShots = originalTimeBetweenShots;
  }
}
