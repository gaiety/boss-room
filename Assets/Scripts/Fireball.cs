using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
  public float speed = 1.0f;
  public AudioSource[] shootSounds;
  private int shootSoundVariations = 5;

  void Start()
  {
    shootSounds[Random.Range(0, shootSoundVariations - 1)].GetComponent<AudioSource>().Play();
  }

  void Update()
  {
    Move();
  }

  void Move()
  {
    transform.position += transform.up * speed * Time.deltaTime * -1;
  }

  void OnTriggerEnter2D(Collider2D collider)
  {
    if (collider.name == "ColliderOuterBoundary") Destroy(gameObject);
  }
}
